if(Drupal.jsEnabled){
	$(document).ready(function(){
		$('#add-case-link')
			.click(function(){
				 var href = $(this).attr('href');
	       href = href + "/AJAX";
				 $.get(href, function(data) {
						$('#add-case-new').html(data).slideDown("normal").find('fieldset.collapsible > legend').each(function() {
              var fieldset = $(this.parentNode);
              // Expand if there are errors inside
              if ($('input.error, textarea.error, select.error', fieldset).size() > 0) {
                fieldset.removeClass('collapsed');
              }
    
              // Turn the legend into a clickable link and wrap the contents of the fieldset
              // in a div for easier animation
              var text = this.innerHTML;
              $(this).empty().append($('<a href="#">'+ text +'</a>').click(function() {
                var fieldset = $(this).parents('fieldset:first')[0];
                // Don't animate multiple times
                if (!fieldset.animating) {
                  fieldset.animating = true;
                  Drupal.toggleFieldset(fieldset);
                }
                return false;
              })).after($('<div class="fieldset-wrapper"></div>').append(fieldset.children(':not(legend)')));
            });
				})
				return false;
		})//end add-case
	}) //end document ready
}
